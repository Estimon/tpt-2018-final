const user = require('../../src/user');

it('uses id number 1', () =>{
    expect(user(1)).toMatchSnapshot();
});

it('uses id number 56', () =>{
    expect(user(56)).toMatchSnapshot();
});

it('uses id number 1345', () =>{
    expect(user(1345)).toMatchSnapshot();
});

it('is a string', () => {
    expect(() =>{
        user('im gonna say the n word. thats racist you cannot say the nword.(Penguins running over an old lady in a truck). Ms Obama i have done it i have stoped racism. Thank you Skipper now i am free to roam this earth. Not when i have to say anything about it and i do im gonna say the nword. Ms Obama get down. NIGGGGGGGAAAAAAA ');
    }).toThrow()  
});


