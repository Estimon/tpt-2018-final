var config = require('../../nightwatch.conf.js');

module.exports = {
    'google test' : function(browser){
        browser
        .url('https://www.google.ee/')
        .waitForElementVisible('body')
        .setValue(`input[class="gLFyf gsfi"]`, ['tallinn', browser.Keys.ENTER])
        .waitForElementVisible('body')
        .assert.containsText('body', 'tallinn')
        .saveScreenshot(`${config.imgpath(browser)}screenshot1.png`)
        .pause(2000)
        .click(`a[href="https://www.tallinn.ee/"]`)
        .saveScreenshot(`${config.imgpath(browser)}screenshot2.png`)
        .pause(2000)
        .end()
        
    }
}